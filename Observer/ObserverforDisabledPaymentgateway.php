<?php
namespace IAGC\DisableCODForCustomerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class ObserverforDisabledPaymentgateway implements ObserverInterface
{
    public function __construct() {}

    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
      $result = $observer->getEvent()->getResult();
      $method_instance = $observer->getEvent()->getMethodInstance();
      $quote = $observer->getEvent()->getQuote();
      /* If Customer  group is match then work */
      if(null !== $quote &&  $quote->getCustomerGroupId() =='5'  ){
          /* Disable All payment gateway  exclude Your payment Gateway*/
          if($method_instance->getCode() == 'cashondelivery'){
               $result->setData('is_available',false);	
          }
      }

    }

}
